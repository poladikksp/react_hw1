import { Component } from "react";
import { Button } from "../components/Button/Button";
import { Modal } from "../components/Modal/Modal";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpened: false, openedModalId: 0 };
  }
  openModal = (e) => {
    this.setState({ modalOpened: true, openedModalId: +e.target.dataset.modalId });
  };
  closeModal = () => {
    this.setState({ modalOpened: false, openedModalId: 0 });
  };
  render() {
    return (
      <>
        <div className="btnWrapper">
          <Button
            modalId="1"
            className="openBtn"
            text="Open first modal"
            backgroundColor="red"
            onClickFn={this.openModal}
          />
          <Button
            modalId="2"
            className="openBtn"
            text="Open second modal"
            backgroundColor="green"
            onClickFn={this.openModal}
          />
        </div>
        {this.state.modalOpened ? <Modal modalId={this.state.openedModalId} closeFn={this.closeModal} /> : null}
      </>
    );
  }
}
