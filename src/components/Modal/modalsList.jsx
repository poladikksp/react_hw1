export const modals = [
  {
    id: 1,
    className: "modal modal_1",
    headerText: "Do you want to delete this file?",
    closeButton: true,
    text: "Once you delete this file it won't be possible to undo this action. Are you sure you want to delete it?",
    buttons: {
      btn1: {
        text: "ok",
      },
      btn2: {
        text: "cancel",
      },
    },
  },
  {
    id: 2,
    className: "modal modal_2",
    headerText: "This is second modal window",
    closeButton: false,
    text: "If you see this modal window after you pressed second button that means everything works great!",
    buttons: {
      btn1: {
        text: "WOW!",
      },
      btn2: {
        text: "Great!",
      },
    },
  },
];
