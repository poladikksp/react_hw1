/* eslint-disable no-useless-constructor */
import { Component } from "react";
import "./button.scss";

export class Button extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <button
        data-modal-id={this.props.modalId}
        className={this.props.className}
        style={{ backgroundColor: this.props.backgroundColor }}
        onClick={this.props.onClickFn}
      >
        {this.props.text}
      </button>
    );
  }
}
